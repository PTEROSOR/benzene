\documentclass[10pt]{letter}
\usepackage{UPS_letterhead,xcolor,mhchem,mathpazo,ragged2e,hyperref}
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\definecolor{darkgreen}{HTML}{009900}


\begin{document}

\begin{letter}%
{To the Editors of the Journal of Chemical Physics}

\opening{Dear Editors,}

\justifying
Please find attached a revised version of the note entitled 
\begin{quote}
\textit{``The performance of CIPSI on the ground state electronic energy of benzene''}.
\end{quote}
We thank the reviewers for their constructive comments.
Our detailed responses to their comments can be found below.
For convenience, changes are highlighted in red in the revised version of the manuscript. 

We look forward to hearing from you.

\closing{Sincerely, the authors.}

%%% REVIEWER 1 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#2}

\begin{itemize}

	\item 
	{As a follow-up note to the recent benchmarking work of Eriksen et al., the authors present the correlation energy of ground state benzene calculated using CIPSI, another flavor in the Selected Configuration Interaction plus Perturbation family. 
	In this endeavor, four combinations regarding the choice of orbitals and perturbation corrections are tested. 
	The best result is obtained from constructing a set of localized orbitals from natural orbitals using a Boys-Foster localization procedure as well as employing a renormalized version of the PT2 correction in the perturbative stage. 
	The final energy agrees with the theoretical estimate of the blind test and is also very close to the best post blind test estimate in the SCI+PT category.
	Benchmarking is indeed an important and essential component in the development of electronic structure theories. 
	It is good that the authors complement the benchmark dataset. 
	Overall, the work is well-motivated and generally explained in a clear and organized manner. 
	However, there are also a few issues to be addressed or clarified. }
	\\
	\alert{We thank the reviewer for his/her positive comments.}

	\item 
	{In the manuscript, the authors reason that rPT2 should be employed because its correction behaves more linearly than its PT2 counterpart. 
	Do authors know of a formal reason to believe that? 
	I believe that the authors have performed numerical tests to confirm this finding in a previous paper and I think should give a citation in the sentence. }
	\\
	\alert{The rPT2 correction corresponds to a partial resummation of some of the higher-order diagrams from many-body perturbation theory.
	We have mentioned this in the revised version of the manuscript.
	As correctly pointed out by the reviewer, this correction has been thoroughly tested in Ref.~51 for weakly and strongly correlated systems, and we have then added this reference to the corresponding sentence as requested.}
	
	\item 
	{It is not very clear how the extrapolation is actually done. 
	The authors mention twice that the result is obtained from a "four-point linear extrapolation". 
	However, according to the right panel of FIG. 1 along with TABLE II, many more than four calculations must be done, which is of course a good thing for extrapolation. 
	Then are the four points randomly selected from a bunch of them? 
	Whatever it is, the authors might want to clarify the confusion.}
	\\
	\alert{Sorry for the confusion. 
	We have taken the last four points which correspond to the four largest variational wave functions.
	This is now clearly stated in the revised manuscript.}
	
	\item 
	{Although a general description of the method is given by the authors, more technical details might be needed to better improve reproducibility, such as values of the thresholds to select the most energetically relevant determinants etc. 
	I realize that this is a note and the authors want to keep it short. In light of this the authors might want to include an input file in their appendix. Because their quantum package is publicly available, it might make it easy for someone to reproduce their findings. }
	\\
	\alert{As suggested by the reviewer, we have made available all our input and output data on an open data repository.
	The data availability statement has been updated accordingly.} 

\end{itemize}

%%% REVIEWER 2 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#3}

\begin{itemize}
	
	\item 
	{In the present Note, the authors Loos, Damour, and Scemama report on the performance of their modern, determinant-driven version of the CIPSI method for the case of the benzene molecule in a standard Dunning correlation-consistent DZ basis set. 
	In particular, the authors compare their results against a recent blind challenge by Eriksen et al., initially deposited on the arXiv preprint server, and now published in JPCL (DOI: 10.1021/acs.jpclett.0c02621).
	While the final findings of the present work are obviously not blinded, it is the belief of this reviewer that these additional results are still valuable as a community resource. 
	The manuscript is reasonably well written (bar the occasional grammatical error/typo, which the authors are sure to locate upon revising the manuscript), and I surely deem it suitable for future publication in JCP. 
	However, I'd like to encourage the authors to take the following concerns and comments into account before submitting a revised version of the manuscript.}
	\\
	\alert{We thank the reviewer for supporting publication of the present manuscript. 
	The references of the blind challenge by Eriksen et al. and the subsequent work of Lee et al. (which was recently published in JCP) have been updated.}
	
	\item 
	{The authors have opted for providing the reader with raw data in tabulated form, which is a real asset. 
	Now, if I were to extrapolate correlated energies myself (for instance, using the polyfit() function of the NumPy library), by means of a weighted fit of the total results to either a linear or a quadratic polynomial in the perturbative correction (using the inverse square of said correction as the weight function), I generally find quite a significant spread in the final results? 
	It would be valuable to the work if the authors were to indicate the variance with respect to the number of points used in the extrapolations; in the linear extrapolations, one could use, say, between 3-5 points, whereas between 4-6 points could be used in the quadratic fits? 
	In any case, the authors should comment (in more detail) on their choice of fitting function and number of data points.}
	\\
	\alert{Using the last 3, 4, 5, and 6 points (i.e., the largest wave functions), linear extrapolations yield the following correlation energy estimates: $-863.1(11)$, $-863.4(5)$, $-862.1(8)$, and $-863.5(11)$ mE$_h$, respectively, where the fitting error is reported in parenthesis. 
	These numbers vary by $1.4$ mE$_h$.
	The four-point extrapolated estimate that we have chosen to report as our best estimate corresponds to the smallest fitting error.
	Quadratic fits yield much larger variations and we never use them in practice.
	All these additional information are now provided in the revised version of the manuscript (see footnote 71).}
	
	\item 
	{As an aside, it would seem like the fifth last point differs ever so slightly from the general trend (regardless of the choice of (r)MP2)? 
	Can the authors explain why? 
	Surely, inclusion of this point in the fitting procedure would bring about changes to the final extrapolated result?}
	\\
	\alert{Yes, the fifth point is slightly off as compared to the others. 
	This is due to the stochastic nature of the calculation of $E_\text{rPT2}$.
	The associated error bars (tabulated in Table II) have a size of the order of the markers and this is now mentioned explicitly in the caption of Fig.~1. 
	As pointed out above, taking into account this fifth point yield a slightly smaller estimate of the correlation energy [$-862.1(8)$ mE$_h$], but taking one more point seem to soften things out [$-863.5(11)$ mE$_h$]. 
	These observations are now mentioned in the same footnote.}
	
	\item 
	{It would be interesting if the authors could comment (even speculatively) on why results in the localized FB basis are significantly lower (and hence, in the authors' own words, more trustworthy) than the corresponding results in the NO basis.}
	\\
	\alert{Localized orbitals significantly speed up the convergence of SCI calculations by taking benefit of the local character of electron correlation.
	We have mentioned this in the revised manuscript and added references discussing the use of localized orbitals \textit{vs} natural orbitals in CI calculations (Refs.~66--70).}
	
	\item 
	{Why are the rMP2-based corrections considered superior to the corresponding corrections based on MP2? 
	Because of the improved linear proportionality in Fig.~1? 
	If so, the authors might want to discuss exactly why a linear relationship is to be expected.}
	\\
	\alert{As mentioned in the original manuscript, the rPT2 correction does indeed yield an improved linear proportionality as compared to the usual PT2 treatment. 
	The theoretical reasons behind this has been clarified in the revised manuscript (see the response to Reviewer \#2).}
	
	\item 
	{Some of the method acronyms have not been properly introduced in the text, and some have been slightly misrepresented, e.g., MBE-FCI (many-body expanded FCI) and FCCR, which is not a selected CC model. 
	Also, some references appear to be missing, e.g., for iCI and DMRG.}
	\\
	\alert{We have more explicitly defined the method acronyms, corrected the description of FCCR, and added the missing references for iCI and DMRG.
	Additional references for FCCR and CAD-FCIQMC have been also added for the sake of completeness.}	
	
	\item 
	{Why are FB orbitals preferred over, e.g., PM orbitals or IBOs?}
	\\
	\alert{Boys-Foster is the only localization criterion available at the moment but we are planning on implementing other schemes in the near future.
	That being said, because we group the MOs by symmetry classes (see footnote 64), our localization procedure ensures the $\sigma$-$\pi$ separability, like in PM.
	This would not be possible in general but, thanks to the high symmetry of benzene, it is feasible in the present case.
	}
	
	\item 
	{The benzene geometry was not optimized as part of the work behind Ref.~17.
	However, an adequate reference may be found in Ref.~17.}
	\\
	\alert{We have added the corresponding reference to the work of Schreiber et al.~and slightly modified the sentence accordingly.}	
\end{itemize}
 
\end{letter}
\end{document}






